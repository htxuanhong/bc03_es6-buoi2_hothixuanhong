let selected = -1;

let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

const xuatDanhSachKinh = () => {
  dataGlasses.forEach(function (item, index) {
    let content = `
        <div class="glasses-left" onclick="clickGlassess(${index})">
            <img src="${item.src}" class="img-glassess" />
        </div>
        `;

    document
      .getElementById("vglassesList")
      .insertAdjacentHTML("beforeend", content);
  });
};

xuatDanhSachKinh();

function clickGlassess(index) {
  // console.log(dataGlasses[index]);
  const item = dataGlasses[index];

  selected = index;

  let content = `
    <div id="glasss-item"> <img src="${item.virtualImg}"></div>`;

  document.getElementById("avatar").innerHTML = content;

  let bottom = `
   <div>
   <div> ${item.name} - ${item.brand} ( ${item.color})</div>
   <div>
   <span style="background-color: red; margin-right: 8px;padding: 4px; border-radius: 4px">$${item.price}  </span> 
   <span style="color: green">Stocking</span>
   </div>
   <div>${item.description}</div>
   </div>
    `;
  document.getElementsByClassName("vglasses__info")[0].style.display = "block";
  document.getElementById("glassesInfo").innerHTML = bottom;
}

function before() {
  selected--;
  if (selected < 0) {
    selected = dataGlasses.length - 1;
  }

  clickGlassess(selected);
}
function after() {
  selected++;
  if (selected == dataGlasses.length) {
    selected = 0;
  }
  clickGlassess(selected);
}
